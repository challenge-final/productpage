FROM python:3.7.7-slim

WORKDIR /opt/microservices

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

COPY test-requirements.txt ./
RUN pip install --no-cache-dir -r test-requirements.txt

COPY productpage.py .
COPY tests/unit/* tests/unit/
COPY templates templates/
COPY static static/

ARG flood_factor
ENV FLOOD_FACTOR=${flood_factor:-0}

EXPOSE 9080

RUN python -m unittest discover

CMD ["python", "productpage.py", "9080"]
